#!/bin/bash

a=$1
b=$2
c=$3
d=$4

linhas=$(wc -l < "$a")
linhas2=$(wc -l < "$b")
linhas3=$(wc -l < "$c")
linhas4=$(wc -l < "$d") 

numlinhas=$linhas
arq=$a

if [ $linhas2 -gt $numlinhas ]; then
	numlinhas=$linhas2
	arq=$b
fi
	
if [ $linhas3 -gt $numlinhas ]; then
	numlinhas=$linhas3	
	arq=$c
fi

if [ $linhas4 -gt $numlinhas ]; then
	numlinhas=$linhas4
	arq=$d
fi

cat $arq
