#!/bin/bash

arq=pix_feito.txt

if [ -e /tmp/$arq ]; then
	echo "está no tmp"

elif [ -e /home/ifpb/$arq ]; then
	echo "está no /home/ifpb"

elif [ -e /proc/$arq ]; then
	echo "está no /proc"

elif [ -e /var/log/$arq ]; then
	echo "está no /var/log"

else 
	echo "o arquivo $arq não exite"
fi
